package com.jmountain.companymanager.service;

import com.jmountain.companymanager.entity.People;
import com.jmountain.companymanager.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class PeopleService {
    private final PeopleRepository peopleRepository;
    public void setPeople(String name, String phone, LocalDate birthday) {
        People addData = new People();
        addData.setName(name);
        addData.setPhone(phone);
        addData.setBirthday(birthday);

        peopleRepository.save(addData);
    }

}
