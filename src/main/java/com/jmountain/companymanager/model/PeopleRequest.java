package com.jmountain.companymanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter//값을 넣고 빼야 할 때
@Setter//값을 넣고 빼야 할 때

public class PeopleRequest {//PeopleRequest = 이름명
    private String name;
    private String phone;
    private LocalDate birthday;

}
