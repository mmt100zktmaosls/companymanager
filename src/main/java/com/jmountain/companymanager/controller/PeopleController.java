package com.jmountain.companymanager.controller;

import com.jmountain.companymanager.model.PeopleRequest;
import com.jmountain.companymanager.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController//Controller의 명찰
@RequiredArgsConstructor//명찰은 꼭 밑에 코드와 붙여 쓰기, 다른 패키지와 소통할 때 붙인다
@RequestMapping("/people")
public class PeopleController {
    private final PeopleService peopleService;//소문자 peopleService는 이름이다

    @PostMapping("/data")
    public String setPeople(@RequestBody PeopleRequest request) {// 안내데스크 이름(사람등록하는사람) : setPeople
        peopleService.setPeople(request.getName(), request.getPhone(), request.getBirthday());//@RequestBody 모델파일의 바디를 만들어준다.(PeopleRequest), request= 이름
        return "OK";//실행확인을 위해서 "OK"를 출력//위에 코드와 연관되어 있기 때문에 코드 줄을 맞춰준다
    } //request의 실행내용 { }


}
