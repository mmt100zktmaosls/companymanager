package com.jmountain.companymanager.repository;

import com.jmountain.companymanager.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People, Long> {//extends 저장소를 확장하게 해준다, People = 타입
    // Long = 값의 크기
}
