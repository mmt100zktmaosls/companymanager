package com.jmountain.companymanager.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class People {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)//@Id = PK( 고유한 값을 할당시켜준다)
    private Long id;//@GeneratedValue(~)PK값을 자동으로 증가시켜준다.
@Column(nullable = false, length = 20)//ex)엑셀 시퀀스=@Column , 값 크기는 length =20
    private String name;
@Column(nullable = false, length = 20)
    private String phone;
@Column(nullable = false, length = 20)
    private LocalDate birthday;//LocalDate는 지역의 날짜이며, Date는 미국의 날짜이다
}
